//
//  Constants.swift
//  fashLOCO
//
//  Created by apple on 24/04/19.
//  Copyright © 2019 apple. All rights reserved.
//

import Foundation
import UIKit

let googleAPIkey =
"194926181162-nks1uuvklp10p9r6mlveer5itpk032e9.apps.googleusercontent.com"

 //MARK:- for Testing Server
//let BaseURL = "https://clientstagingdev.com/kinoplastic_server/api/"

//MARK:- for Testing Server
//let BaseURL = "http://68.183.74.38:8008/joao/api/"

//MARK:- for live Server
let BaseURL = "https://diggy.tech/api/"
 
//var mylanguagecode = "pt-BR"
let mylanguagecode = "en"

public enum methodsName{
    public enum userCase{
        case userSignIn
        case userSignUp
        case forgetPassword
        case getProfile
        case editProfile
        case changePassword
        case logout
        case termsConditions
        case privacyPolicy
        case ChooseBrand
        case GetCompaigns
        case GetCompaignDetails
        case GetallState
        case GetallCity
        case GetallLocation
        case GetSupporttittle
        case submitSupport
        case GiftVouncherList
        case RedeemGiftVoucher
        case MySpace
        case TerminateAccount
        case ClaimPointsUsingQrcodes
        case validCPF
        case GetNewsLetter
        case uploadImage
        var caseValue : String {
            switch self {
            case .userSignUp:           return "auth/register"
            case .userSignIn:           return "auth/login"
            case .forgetPassword:       return "auth/forgotPassword"
            case .getProfile:           return "auth/getProfile"
            case .editProfile:          return "auth/editProfile"
            case .changePassword:       return "auth/changePassword"
            case .logout :              return "auth/logout"
            case .termsConditions:      return "auth/termsConditions"
            case .privacyPolicy:        return "auth/privacyPolicy"
            case .ChooseBrand:          return "auth/getAllBrands"
            case .GetCompaigns:         return "campaign/getLocationCampaigns"
            case .GetCompaignDetails:   return "campaign/getCampaignDetail"
            case .GetallState:          return "auth/getAllState"
            case .GetallCity:           return "auth/getAllCity"
            case .GetallLocation:       return "auth/getAllLocations"
            case .GetSupporttittle:     return "support/getTitle"
            case .submitSupport:        return "support/generateTicket"
            case .GiftVouncherList:     return "voucher/getVoucherList"
            case .RedeemGiftVoucher:    return "voucher/redeem"
            case .MySpace:              return "mySpace"
            case .TerminateAccount:     return "auth/terminateAccount"
            case .ClaimPointsUsingQrcodes: return "campaign/claimCampaignPoints"
            case . validCPF:             return "auth/validatecpf"
            case .GetNewsLetter:         return "notificationlist"
            case .uploadImage:          return "uploadimage"
            }
        }
    }
}


let FONT_COLOR = UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 0.5)

let THEME_COLOR = UIColor(red: 0/255.0, green: 94/255.0, blue: 126/255.0, alpha: 1.0)

let YELLOW_COLOR = UIColor(red: 255/255.0, green: 199/255.0, blue: 0/255.0, alpha: 1.0)

let CELL_SLECTED_COLOR = UIColor(red: 247/255.0, green: 247/255.0, blue: 247/255.0, alpha: 1.0)

let GREY_FONT_COLOR = UIColor(red: 173/255.0, green: 172/255.0, blue: 173/255.0, alpha: 1.0)
let Line_View_colour = UIColor(red: 194/255.0, green: 234/255.0, blue: 245/255.0, alpha: 1.0)
let for_ground_colour = UIColor(red: 20/255.0, green: 155/255.0, blue: 199/255.0, alpha: 1.0)

let Points_Earned = [["50","30 April, 2019","24 June, 2019","Approved"],["45","25 April, 2019","20 June, 2019","Approved"],["5","3 April, 2019","2 June, 2019","Approved"],["50","30 April, 2019","24 June, 2019","Approved"],]

let Points_Spent = [["30 April, 2019","Multiplus","$1000","50"],["3 April, 2019","Multiplus","$100","5"],["15 April, 2019","Multiplus","$660","80"],["23 April, 2019","Multiplus","$100","5"]]

let Support_Questions = ["question1","question2","question3","question4","question5","question6"]

let Starting_Ads_images = [UIImage(named: "asset_5"),UIImage(named: "asset_6"),UIImage(named: "asset_7")] as! [UIImage]

let Side_menu_name = ["Home","Invites","Notifications","Chat","Badges","Wardrobe","Settings","Logout"]

let Side_menu_white_images = [UIImage(named: "homewhite"),UIImage(named: "invitewhite"),UIImage(named: "notiwhite"),UIImage(named: "chatwhite"),UIImage(named: "badgewhite"),UIImage(named: "wardrobewhite"),UIImage(named: "settingwhite"),UIImage(named: "logoutwhite")] as! [UIImage]

let Side_menu_red_images = [UIImage(named: "homered"),UIImage(named: "Invitered"),UIImage(named: "notired"),UIImage(named: "chatred"),UIImage(named: "badgered"),UIImage(named: "wardrobered"),UIImage(named: "settingred"),UIImage(named: "logoutred")] as! [UIImage]

let Dummy_Friends_name = ["Jack","Jan Kanza","Thomas","Linnea","Jack","Apollonia","Jack","Jan Kanza","Jack","Jan Kanza"]

let UserTypeArray = ["Vendedor","Comprador","Gerente"]
