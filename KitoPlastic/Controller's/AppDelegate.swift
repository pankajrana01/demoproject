//
//  AppDelegate.swift
//  KitoPlastic
//
//  Created by apple on 03/05/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift


import Firebase
import UserNotifications
import FirebaseMessaging
import FirebaseInstanceID

@available(iOS 11.0, *)
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,MessagingDelegate,UNUserNotificationCenterDelegate{
       
    
    
    var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_id"
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        
        // Override point for customization after application launch.
        IQKeyboardManager.shared.enable = true
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        let backgroundImage = UIImage(named: "navigationBG")?.resizableImage(withCapInsets: UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15), resizingMode: UIImage.ResizingMode.stretch)
        UINavigationBar.appearance().setBackgroundImage(backgroundImage, for: .default)
        
        
        
        
        
        
        // ================Request user permission for device token======================
        let notificationTypes: UIUserNotificationType = [UIUserNotificationType.alert,UIUserNotificationType.badge, UIUserNotificationType.sound]
        let pushNotificationSettings = UIUserNotificationSettings(types: notificationTypes, categories: nil)
        application.registerUserNotificationSettings(pushNotificationSettings)
        application.registerForRemoteNotifications()
        
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent
        
        //fire notification
        application.registerForRemoteNotifications()
        //firebase setup
        FirebaseApp.configure()
        // [START set_messaging_delegate]
        Messaging.messaging().delegate = self
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert,.sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        let token = Messaging.messaging().fcmToken
        print("FCM token: \(token ?? "")")
        
        // This will enable to show nowplaying controls on lock screen
        application.beginReceivingRemoteControlEvents()
        
      sleep(2)
        
        return true
    }
    
    //notification
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        if let messageID = userInfo[gcmMessageIDKey] {
            //  print("Message ID: \(messageID)")
        }
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        if let messageID = userInfo[gcmMessageIDKey] {
            // print("Message ID: \(messageID)")
        }
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    
    // This method will be called when app received push notifications in foreground
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {
        let userInfo = notification.request.content.userInfo
        print(userInfo)
        let userinfo = userInfo as NSDictionary
        if userinfo.value(forKey: "user_type")as! String == "notification"{
            let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let apptVC = storyboard.instantiateViewController(withIdentifier: "TabBarController") as! TabBarController
            let navigationController = UINavigationController.init(rootViewController: apptVC)
            self.window?.rootViewController = navigationController
            self.window?.makeKeyAndVisible()
}
         completionHandler([.alert, .badge, .sound])
    }
    
    // Handle notification messages after display notification is tapped by the user.
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        print(userInfo)
        let userinfo = userInfo as NSDictionary
        if userinfo.value(forKey: "user_type")as! String == "notification"{
            let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let apptVC = storyboard.instantiateViewController(withIdentifier: "TabBarController") as! TabBarController
            let navigationController = UINavigationController.init(rootViewController: apptVC)
            self.window?.rootViewController = navigationController
            self.window?.makeKeyAndVisible()
            
        }
    }
   // The callback to handle data message received via FCM for devices running iOS 10 or above.
    func application(received remoteMessage: MessagingRemoteMessage) {
        print(remoteMessage.appData)
    }
    
    //======================Getting device token========================
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        let token = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
        print("your device token is \(token)")
        UserDefaults.standard.set(String(token), forKey: "DeviceToken")
        UserDefaults.standard.synchronize()
//        // Get FCM Token
//        InstanceID.instanceID().instanceID(handler: { (result, error) in
//            if let error = error {
//                print("Error fetching remote instange ID: \(error)")
//            } else if let result = result {
//                print("Remote instance ID token: \(result.token)")
//                let fcmToken = "\(result.token)"
//                UserDefaults.standard.set(fcmToken, forKey: "FCM_TOKEN")
//            }
//        })
    }
    
    // ===================In case of error========================
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        UserDefaults.standard.set("12952929265256561561561361261162", forKey: "DeviceToken")
        UserDefaults.standard.synchronize()
        print("i am not available in simulator \(error)")
    }
    
    // [START refresh_token]
    internal func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        
        print("Firebase registration token: \(fcmToken)")
        UserDefaults.standard.set(fcmToken, forKey: "FCM_TOKEN")
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        UserDefaults.standard.set(fcmToken, forKey: "FCM_TOKEN")
        
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        
        //MARK:- for Network Reachiblity
        let reachability = PMDReachabilityWrapper.sharedInstance()
        reachability?.monitorReachability()
        
        
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        //MARK:- for Network Reachiblity
        let reachability = PMDReachabilityWrapper.sharedInstance()
        reachability?.monitorReachability()
        
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    // Google signIN delegate method
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        
       return true
    }
    
}


