//
//  NotificationsVC.swift
//  fashLOCO
//
//  Created by Ashish_IOS on 4/25/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

class NotificationCell:UITableViewCell{
    
    @IBOutlet weak var namelbl: UILabel!
    @IBOutlet weak var timelbl: UILabel!
}
@available(iOS 11.0, *)
class NotificationsVC: UIViewController {

    var mylanguageCode = "pt-BR"
    override func viewDidLoad() {
        super.viewDidLoad()
        addNavigationBarImage()
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.title = "Notification".localized(loc: mylanguageCode)
        self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        // Do any additional setup after loading the view.
    }
 
}
@available(iOS 11.0, *)
extension NotificationsVC:UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell") as! NotificationCell
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}
