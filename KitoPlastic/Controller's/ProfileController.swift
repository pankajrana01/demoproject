//
//  ProfileController.swift
//  Conselho Fashion
//
//  Created by apple on 07/05/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import Alamofire
struct GlobalClass {
    static var ischanged : Int = 0
    static var locationID  : String = ""
}
@available(iOS 11.0, *)
class ProfileController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate,LBZSpinnerDelegate,UITextFieldDelegate {
    // MARK:-  Spinner Choose
    func spinnerChoose(_ spinner: LBZSpinner, index: Int, value: String) {
        if spinner == BrandView{
            
            BrandView.text = value
            chooseState()
            stateView.text = "ChooseState".localized(loc: mylanguageCode)
            CityView.text = "ChooseCity".localized(loc: mylanguageCode)
            locationView.text = "ChooseLocation".localized(loc: mylanguageCode)
            //myBrandArray = chooselocation[index].brand
            // statelbzView.updateList(myBrandArray)
        }
        else if spinner == stateView{
            stateView.text = value
            chooseCity()
            CityView.text = "ChooseCity".localized(loc: mylanguageCode)
            locationView.text = "ChooseLocation".localized(loc: mylanguageCode)
        }
        else if spinner == CityView{
            CityView.text = value
            chooseLocation()
            locationView.text = "ChooseLocation".localized(loc: mylanguageCode)
        }
        else {
            locationView.text = value
            mylocationId = locationIDArray[index]
            MySelectedLocation = locationChecked[index]
            if SingletonClass.sharedInstance.usertype == "1"{
                print("user type is 1")
            }
            else{
                if MySelectedLocation == "0"{
                    //Update Dictionary
                    self.LocationDict.updateValue(BrandView.text, forKey: "Brand")
                    self.LocationDict.updateValue(stateView.text, forKey: "State")
                    self.LocationDict.updateValue(CityView.text, forKey: "City")
                    self.LocationDict.updateValue(locationView.text, forKey: "Location")
                    self.LocationDict.updateValue(mylocationId, forKey: "LocationID")
                    
                     let newDictionary = LocationDict
                    if !LocationArray.contains{ $0 == newDictionary } {
                        LocationArray.append(LocationDict)
                        
                        print("my new Location array is:\(LocationArray)")
                       
                        locationCollectionView.delegate = self
                        locationCollectionView.dataSource = self
                        locationCollectionView.reloadData()
                    }
                    else{
                        print("your Current location is already exist in locationarray")
                    }
                }
                else{
                    locationView.text = "ChooseLocation".localized(loc: mylanguageCode)
                    self.ShowAlertView(title: "Conselho Fashion", message: "Local já selecionado por outro usuário.", viewController: self)
                }
              }
        }
    }
    
    @IBOutlet weak var save: UIButton!
    @IBOutlet weak var blurImg: UIImageView!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var editView: UIView!
    @IBOutlet weak var view1: DesignableView!
    @IBOutlet weak var view2: DesignableView!
    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var userCPF: UITextField!
    @IBOutlet weak var userContact: UITextField!
    
    @IBOutlet weak var userEmail: UITextField!
    @IBOutlet weak var profileCircleImg: DesignableImage!
    @IBOutlet weak var edit_userName: UITextField!
    @IBOutlet weak var edit_UserCPF: UITextField!
    @IBOutlet weak var edit_userContact: UITextField!
    
    @IBOutlet weak var edit_userEmail: UITextField!
    @IBOutlet weak var BrandView: LBZSpinner!
    @IBOutlet weak var EbrandTextField: UITextField!
    @IBOutlet weak var stateView: LBZSpinner!
    @IBOutlet weak var EStateTF: UITextField!
    @IBOutlet weak var ECityTF: UITextField!
    @IBOutlet weak var CityView: LBZSpinner!
    @IBOutlet weak var ELocationTF: UITextField!
    @IBOutlet weak var locationView: LBZSpinner!
    @IBOutlet weak var BrandTF: UITextField!
    @IBOutlet weak var stateTF: UITextField!
    @IBOutlet weak var cityTF: UITextField!
    @IBOutlet weak var locationTF: UITextField!
    @IBOutlet weak var usernameText: UILabel!
    @IBOutlet weak var cpfText: UILabel!
    @IBOutlet weak var contactText: UILabel!
    @IBOutlet weak var BrandText: UILabel!
    @IBOutlet weak var stateText: UILabel!
    @IBOutlet weak var CityText: UILabel!
    @IBOutlet weak var locationText: UILabel!
    @IBOutlet weak var emailText: UILabel!
    
    
    @IBOutlet weak var EusernameText: UILabel!
    @IBOutlet weak var ECpfText: UILabel!
    @IBOutlet weak var EContactText: UILabel!
    @IBOutlet weak var EBrandText: UILabel!
    @IBOutlet weak var EStateText: UILabel!
    @IBOutlet weak var ECityText: UILabel!
    @IBOutlet weak var ELocationText: UILabel!
    @IBOutlet weak var locationCollectionView: UICollectionView!
    @IBOutlet weak var EEmailText: UILabel!
    @IBOutlet weak var locationTextview: UITextView!
    
    
    // local variables
    var profileDictionary : NSDictionary = [:]
    var MyImage : UIImage!
    var imagePicker:UIImagePickerController?=UIImagePickerController()
    var name = [String]()
    var State = [String]()
    var City = [String]()
    var location = [String]()
    var locationIDArray = [String]()
    
    var mylocationId = String()
    var mylanguageCode = "pt-BR"
    var MyLocationName = [String]()
    var LocationArray = [[String:String]]()
    var LocationDict = ["Brand":"",
                        "State":"",
                        "City":"",
                        "Location":"",
                        "LocationID":""]
    var usertype = ""
    var MySelectedLocation = ""
     var locationChecked = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DidloadFunction()  // Function call when user come profile page
    }
    
    override func viewWillAppear(_ animated: Bool) {
        changeStoryboardWithLanguage()
    }
    
    func DidloadFunction(){
        CityView.delegate = self
        BrandView.delegate = self
        locationView.delegate = self
        stateView.delegate = self
        BrandView.textColor = .darkGray
        stateView.textColor = .darkGray
        CityView.textColor = .darkGray
        locationView.textColor = .darkGray
        if UserDefaults.standard.value(forKey: "socialValue") as? String == "5"{
            editBtn.isHidden = false
            editView.isHidden = true
            view1.isHidden = false
            view2.isHidden = true
            getProfile()
        }
        else{
            editBtn.isHidden = false
            self.navigationController?.isNavigationBarHidden = true
            editView.isHidden = true
            view1.isHidden = false
            view2.isHidden = true
            // user ProfileData Function Call
            getProfile()
        }
    }
    
    //MARK:- Textfield delegate method
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let quest:String = "+55" // Character to append
        let Hiphen : Character = "-"
        let char = string.cString(using: String.Encoding.utf8)
        let isBackSpace = strcmp(char, "\\b")
        if (isBackSpace == -92) {
            print("Backspace was pressed")
        }
        
        if edit_userContact.text?.count == 0 {
            if (isBackSpace == -92) {
                print("Backspace was pressed")
            }
            else{
                self.edit_userContact.text!.append(quest) // append "+"
            }
            
        }
        if edit_userContact.text?.count == 6 { // if textfield has exactly one character
            if (isBackSpace == -92) {
                print("Backspace was pressed")
            }
            else{
                self.edit_userContact.text!.append(Hiphen) // append "+"
            }
        }
        if edit_userContact.text?.count == 12 { // if textfield has exactly one character
            if (isBackSpace == -92) {
                print("Backspace was pressed")
            }
            else{
                self.edit_userContact.text!.append(Hiphen) // append "+"
            }
        }
        let currentCharacterCount = edit_userContact.text?.count ?? 0
        if range.length + range.location > currentCharacterCount {
            return false
        }
        let newLength = currentCharacterCount + string.count - range.length
        return newLength <= 17
        
    }
    
    //MARK:- --------------------- Get profileData API----------------------------------------
    func getProfile(){
       
        showProgress()
        networkServices.shared.postDataWithoutParameter(methodname: methodsName.userCase.getProfile.caseValue) { (response) in
            print(response)
            self.hideProgress()
            let dic = response as! NSDictionary
            print (dic)
            if let invalidtoken = dic.value(forKey: "message") as? String{
                if invalidtoken == "Invalid token."{
                    // Show alert View
                    let alertController = UIAlertController(title: "Conselho Fashion", message: "InvalidToken".localized(loc: self.mylanguageCode), preferredStyle: .alert)
                    let acceptAction = UIAlertAction(title: "OK", style: .default) { (_) -> Void in
                        UserDefaults.standard.removeObject(forKey: "userid")
                        UserDefaults.standard.removeObject(forKey: "usertoken")
                        // UserDefaults.standard.removeObject(forKey: "socialValue")
                        let Home = self.storyboard?.instantiateViewController(withIdentifier: "SignInController") as! SignInController
                        self.present(Home, animated:true, completion:nil)
                    }
                    alertController.addAction(acceptAction)
                    self.present(alertController, animated: true, completion: nil)
                }
                else if dic.value(forKey: "success") as! Bool == true {
                    if let profileDictionary = dic.value(forKey: "data") as? NSDictionary{
                        print(profileDictionary)
                        // get value from profileDictionay and put in to profile
                        if profileDictionary.value(forKey: "profile_pic") as! String == "" {
                            self.profileImg.image = #imageLiteral(resourceName: "Asset 15")
                        }
                        else{
                            let profileImage = (profileDictionary.value(forKey: "profile_pic") as! String)
                            let profileImageUrl = URL(string: profileImage)
                            if let profile = try? Data(contentsOf: profileImageUrl!)
                            {
                                let image: UIImage = UIImage(data: profile)!
                                self.profileImg.image = image
                                self.profileCircleImg.image = image
                                self.blurImg.image = image
                            }
                        }
                        if profileDictionary.value(forKey: "username") != nil{
                            self.userName.text = (profileDictionary.value(forKey: "username") as! String)
                            self.edit_userName.text = (profileDictionary.value(forKey: "username") as! String)
                        }
                        else{
                            print("user Does not have any user name")
                        }
                        if profileDictionary.value(forKey: "phone_number") != nil{
                            self.userContact.text = (profileDictionary.value(forKey: "phone_number") as? String)
                            self.edit_userContact.text = (profileDictionary.value(forKey: "phone_number") as? String)
                        }
                        else{
                            print("user Does not have any phone name")
                        }
                        self.usertype = (profileDictionary.value(forKey: "user_type") as! String)
                        if self.usertype == "1"{
                            if profileDictionary.value(forKey: "address") != nil{
                                self.locationTextview.text = (profileDictionary.value(forKey: "address_field") as? String)
                                self.locationView.text = ((profileDictionary.value(forKey: "address_field") as? String)!)
                                self.mylocationId = (profileDictionary.value(forKey: "address") as! String)
                            }
                            else{
                                print("user Does not have any location")
                            }
                            if profileDictionary.value(forKey: "brand") != nil{
                                //BrandTF.text = (profileDictionary.value(forKey: "brand") as? String)
                                self.BrandView.text = ((profileDictionary.value(forKey: "brand") as? String)!)
                            }
                            else{
                                print("user Does not have any Brand")
                            }
                            if profileDictionary.value(forKey: "state") != nil{
                                // stateTF.text = (profileDictionary.value(forKey: "state") as? String)
                                self.stateView.text = ((profileDictionary.value(forKey: "state") as? String)!)
                            }
                            else{
                                print("user Does not have any state")
                            }
                            if profileDictionary.value(forKey: "city") != nil{
                                //cityTF.text = (profileDictionary.value(forKey: "city") as? String)
                                self.CityView.text = ((profileDictionary.value(forKey: "city") as? String)!)
                            }
                            else{
                                print("user Does not have any city")
                            }
                            self.ChooseBrand() // API Call
                        }
                        else{
                            self.locationIDArray.removeAll()
                            self.MyLocationName.removeAll()
                            self.LocationArray = profileDictionary.value(forKey: "user_Location") as! [[String:String]]
                            print("my location array is :- \(self.LocationArray)")
                            for LocationDict in self.LocationArray{
                                if let LocationName = LocationDict["Location"]{
                                    self.MyLocationName.append(LocationName)
                                }
                            }
                            self.locationTextview.text = self.MyLocationName.joined(separator: ",")
                            
                            // get last selected value from location array and show that
                            let mylastSelectedArray = self.LocationArray.last
                            
                            
                            self.CityView.text = mylastSelectedArray!["City"]!
                            self.BrandView.text = mylastSelectedArray!["Brand"]!
                            self.stateView.text = mylastSelectedArray!["State"]!
                            self.locationView.text = mylastSelectedArray!["Location"]!
                            
                            self.ChooseBrand() // API Call
                            
                            self.locationCollectionView.delegate = self
                            self.locationCollectionView.dataSource = self
                            self.locationCollectionView.reloadData()
                        }
                        self.userEmail.text = (profileDictionary.value(forKey: "email") as? String)
                        //edit_userEmail.text = (profileDictionary.value(forKey: "email") as? String)
                        if profileDictionary.value(forKey: "cpf_number") != nil{
                            self.userCPF.text = (profileDictionary.value(forKey: "cpf_number") as? String)
                            self.edit_UserCPF.text = (profileDictionary.value(forKey: "cpf_number") as? String)
                        }
                        else{
                            print("user Does not have any CPF number")
                        }
                    }
                    
                }
                else{
                    self.hideProgress()
                    self.ShowAlertView(title: "Conselho Fashion", message: dic.value(forKey: "message") as! String, viewController: self)
                }
            }
        }
    }
    
    //MARK:- language Translate
    func changeStoryboardWithLanguage(){
        save.setTitle("Save".localized(loc: mylanguageCode), for: .normal)
        EusernameText.text = "userName".localized(loc: mylanguageCode)
        ECpfText.text = "CPF".localized(loc: mylanguageCode)
        EContactText.text = "contact".localized(loc: mylanguageCode)
        EBrandText.text = "Brand".localized(loc: mylanguageCode)
        EStateText.text = "State".localized(loc: mylanguageCode)
        ECityText.text = "City".localized(loc: mylanguageCode)
        //EEmailText.text = "Email".localized(loc: mylanguageCode)
        usernameText.text = "userName".localized(loc: mylanguageCode)
        cpfText.text = "CPF".localized(loc: mylanguageCode)
        contactText.text = "contact".localized(loc: mylanguageCode)
        //BrandText.text = "Brand".localized(loc: mylanguageCode)
        //stateText.text = "State".localized(loc: mylanguageCode)
        //CityText.text = "City".localized(loc: mylanguageCode)
        emailText.text = "Email".localized(loc: mylanguageCode)
        
    }
    @IBAction func backBtn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func editBtnAct(_ sender: UIButton) {
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        blurImg.addSubview(blurEffectView)
        editView.isHidden = false
        editBtn.isHidden = true
        view1.isHidden = true
        view2.isHidden = false
    }
    
    @IBAction func saveBtn(_ sender: UIButton) {
        self.view.endEditing(true)
         if BrandView.text == "ChooseBrand".localized(loc: mylanguageCode){
            self.ShowAlertView(title: "Alert", message: "stateAlert".localized(loc: mylanguageCode), viewController: self)
        }
           else if stateView.text == "ChooseState".localized(loc: mylanguageCode){
                self.ShowAlertView(title: "Alert", message: "stateAlert".localized(loc: mylanguageCode), viewController: self)
            }
            else if CityView.text == "ChooseCity".localized(loc: mylanguageCode){
                self.ShowAlertView(title: "Alert", message: "CityAlert".localized(loc: mylanguageCode), viewController: self)
            }
            else if locationView.text == "ChooseLocation".localized(loc: mylanguageCode){
                self.ShowAlertView(title: "Alert", message: "LocationAlert".localized(loc: mylanguageCode), viewController: self)
            }
         else{
             uploadProfile() // Register API function call
        }
       
        
        //save.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(buttonTapped)))
    }
    
    
    
    
    @IBAction func openGallery(_ sender: UIButton) {
        CameraActionSheet()
    }
    
    @IBAction func EditBackTap(_ sender: UIButton) {
        self.editView.isHidden = true
        self.editBtn.isHidden = false
        self.view1.isHidden = false
        self.view2.isHidden = true
        
    }
    
    //MARK:- ================================= Action Sheet to open camera and gallery=========================
    func CameraActionSheet(){
        let optionMenu = UIAlertController(title: nil, message: "ChooseOption".localized(loc: mylanguageCode), preferredStyle: .actionSheet)
        let TakeAction = UIAlertAction(title: "TakePhoto".localized(loc: mylanguageCode), style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.opencamera()
        })
        let ChooseAction = UIAlertAction(title: "ChoosePhoto".localized(loc: mylanguageCode), style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.openGallery()
        })
        let cancelAction = UIAlertAction(title: "Cancel".localized(loc:mylanguageCode), style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        // Add the actions
        imagePicker?.delegate = self
        optionMenu.addAction(TakeAction)
        optionMenu.addAction(ChooseAction)
        optionMenu.addAction(cancelAction)
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    //MARK: ==============================Function to open Camera====================
    func opencamera()
    {
        if UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            imagePicker!.delegate = self
            imagePicker!.sourceType = UIImagePickerController.SourceType.camera
            imagePicker!.allowsEditing = true
            imagePicker!.cameraCaptureMode = UIImagePickerController.CameraCaptureMode.photo;
            self.present(imagePicker!, animated: true, completion: nil)
        }else{
            ShowAlertView(title: "Conselho Fashion", message: "problemCamera".localized(loc: mylanguageCode), viewController: self)
        }
    }
    //MARK:================= Function to open Gallery================================
    
    func openGallery()
    {
        if UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary) {
            imagePicker!.delegate = self
            imagePicker!.sourceType = UIImagePickerController.SourceType.photoLibrary;
            imagePicker!.allowsEditing = true
            self.present(imagePicker!, animated: true, completion: nil)
        }
    }
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let image = info[.editedImage] as? UIImage else { return }
        print(image.size)
        profileCircleImg.image = image
        blurImg.image = image
        MyImage = image
        profileCircleImg.layer.cornerRadius = profileCircleImg.frame.size.height/2
        profileCircleImg.clipsToBounds = true
        dismiss(animated: true)
    }
    
    //MARK:- perform action when user tap  Location cross Button Tap
    @IBAction func crossBtnTap(_ sender: UIButton) {
        
        let buttonPostion = sender.convert(sender.bounds.origin, to: locationCollectionView)
        
        if let indexPath = locationCollectionView.indexPathForItem(at: buttonPostion) {
            let rowIndex =  indexPath.row
            print("Button Tapped at indexpath:- \(rowIndex)")
            let ArrayDict : [[String:Any]] = [self.LocationArray.remove(at: rowIndex)]
            print(ArrayDict)
            locationCollectionView.reloadData()
            if LocationArray.count == 0{
               
                BrandView.text = "ChooseBrand".localized(loc: "pt-BR")
                stateView.text = "ChooseState".localized(loc: "pt-BR")
                CityView.text = "ChooseCity".localized(loc: "pt-BR")
                locationView.text = "ChooseLocation".localized(loc: "pt-BR")
            }
            else{
                
            }
        }
    }
    
    // MARK:- ================Register Data on Server With Alarmofire without profile pic=============================
    func uploadProfile(){
        var parameter : [String : Any]
        self.showProgress()
        if usertype == "1"{
            parameter = [
                "username" : edit_userName.text!,
                "phone_number" : edit_userContact.text!,
                "brand" :   BrandView.text,
                "cpf_number": edit_UserCPF.text!,
                "address" : self.mylocationId,
                "state": stateView.text,
                "city":CityView.text,
                "user_type" : usertype]
            
        }
        else{
            parameter = [
                "username" : edit_userName.text!,
                "phone_number" : edit_userContact.text!,
                "user_Location" : self.LocationArray,
                "cpf_number": edit_UserCPF.text!,
                "user_type" : usertype]
        }
        print(parameter)
        
        networkServices.shared.postDatawithHeader(methodName: methodsName.userCase.editProfile.caseValue, parameter: parameter ) { (response) in
            print(response)
            let dic = response as! NSDictionary
            print (dic)
            self.hideProgress()
            if dic.value(forKey: "success") as! Bool == true
            {
                if self.MyImage == nil{
                    if let data = dic.value(forKey: "data") as? NSDictionary{
                        print(data)
                        
                        // Show alert View
                        let alertController = UIAlertController(title: "Conselho Fashion", message: "profileUpdasted".localized(loc: self.mylanguageCode), preferredStyle: .alert)
                        let acceptAction = UIAlertAction(title: "OK", style: .default) { (_) -> Void in
                            self.getProfile()
                            self.editView.isHidden = true
                            self.editBtn.isHidden = false
                            self.view1.isHidden = false
                            self.view2.isHidden = true
                        }
                        alertController.addAction(acceptAction)
                        self.present(alertController, animated: true, completion: nil)
                    }
                }
                else{
                    self.UploadImage()
                }
            }
            else{
                self.hideProgress()
                
                if let msg = dic.value(forKey : "message") as? String {
                    self.ShowAlertView(title: "Conselho Fashion", message: msg, viewController: self)
                }
                
                // self.ShowAlertView(title: "Conselho Fashion", message: dic.value(forKey: "message") as! String, viewController: self)
            }
        }
    }
    
    // MARK :-====================== Upload image on server=============================
    func UploadImage(){
        showProgress()
        let image = MyImage
        let myImgData = image!.jpegData(compressionQuality: 0.75)
        
        let methodname = methodsName.userCase.uploadImage.caseValue
        let userID = UserDefaults.standard.value(forKey: "userid") as! String
        networkServices.shared.updateMultipart(action: methodname, param: ["userid":userID],imageData: myImgData!, success: { (response) in
            print(response)
            let dict:[String:Any] = response as! [String : Any]
            if let sucess = dict["success"] as? Bool{
                if sucess == true{
                    // Show alert View
                    let alertController = UIAlertController(title: "Conselho Fashion", message: "profileUpdasted".localized(loc: self.mylanguageCode), preferredStyle: .alert)
                    let acceptAction = UIAlertAction(title: "OK", style: .default) { (_) -> Void in
                        self.getProfile()
                        self.editView.isHidden = true
                        self.editBtn.isHidden = false
                        self.view1.isHidden = false
                        self.view2.isHidden = true
                    }
                    alertController.addAction(acceptAction)
                    self.present(alertController, animated: true, completion: nil)
                    
                }else{
                    if let message = dict["message"] as? String{
                        self.ShowAlertView(title: "Conselho Fashion", message: message, viewController: self)
                    }
                }
            }
        }) { (error) in
            self.hideProgress()
            print(error)
            self.ShowAlertView(title: "Conselho Fashion", message: error.localizedDescription, viewController: self)
        }
        
    }
    
    
    
    //MARK:- ==============Choose location Brand City State API
    //MARK:- =========================Choose Brands on Server API==================
    func ChooseBrand(){
        print("Brand API Call")
        networkServices.shared.postDatawithoutHeaderWithoutParameter(methodName: methodsName.userCase.ChooseBrand.caseValue) { (response) in
            print(response)
            let dic = response as! NSDictionary
            print(dic)
            if dic.value(forKey: "success") as! Bool == true{
                if let data = dic.value(forKey: "data") as? NSArray{
                    print(data)
                    self.name.removeAll()
                    for i in 0..<data.count{
                        self.name.append((data[i] as AnyObject).value(forKey: "brand") as! String)
                    }
                    self.BrandView.updateList(self.name)
                    self.chooseState()
                    print(self.name)
                }
            }
            //            else{
            //                self.ShowAlertView(title: "Conselho Fashion", message: dic.value(forKey: "message") as! String, viewController: self)
            //            }
        }
    }
    
    //MARK:- =========================choose State on Server API==================
    func chooseState(){
        print("State API Call")
        let parameter = ["brand" : BrandView.text]
        print(parameter)
        networkServices.shared.postDatawithoutHeader(methodName: methodsName.userCase.GetallState.caseValue, parameter: parameter) { (response) in
            print(response)
            let dic = response as! NSDictionary
            print(dic)
            if dic.value(forKey: "success") as! Bool == true{
                if let data = dic.value(forKey: "data") as? NSArray{
                    print(data)
                    self.State.removeAll()
                    for i in 0..<data.count{
                        self.State.append((data[i] as AnyObject).value(forKey: "state") as! String)
                    }
                    self.stateView.updateList(self.State)
                    self.chooseCity()
                }
            }
            //            else{
            //                self.ShowAlertView(title: "Conselho Fashion", message: dic.value(forKey: "message") as! String, viewController: self)
            //            }
        }
    }
    //MARK:- =========================Choose City on Server API==================
    func chooseCity(){
         print("City API Call")
        let parameter = ["state" : stateView.text,
                         "brand" : BrandView.text]
        print(parameter)
        networkServices.shared.postDatawithoutHeader(methodName: methodsName.userCase.GetallCity.caseValue, parameter: parameter) { (response) in
            print(response)
            let dic = response as! NSDictionary
            print(dic)
            if dic.value(forKey: "success") as! Bool == true{
                if let data = dic.value(forKey: "data") as? NSArray{
                    print(data)
                    self.City.removeAll()
                    for i in 0..<data.count{
                        self.City.append((data[i] as AnyObject).value(forKey: "city") as! String)
                        
                    }
                    self.CityView.updateList(self.City)
                    self.chooseLocation()
                }
            }
            //            else{
            //                self.ShowAlertView(title: "Conselho Fashion", message: dic.value(forKey: "message") as! String, viewController: self)
            //            }
        }
    }
    //MARK:- =========================Choose City on Server API==================
    func chooseLocation(){
        print("Location API Call")
        let parameter = ["city" : CityView.text,
                         "brand" : BrandView.text,
                         "user_type" : usertype]
        print(parameter)
        networkServices.shared.postDatawithoutHeader(methodName: methodsName.userCase.GetallLocation.caseValue, parameter: parameter) { (response) in
            print(response)
            let dic = response as! NSDictionary
            print(dic)
            if dic.value(forKey: "success") as! Bool == true{
                if let data = dic.value(forKey: "data") as? NSArray{
                    print(data)
                    self.location.removeAll()
                    self.locationIDArray.removeAll()
                    self.locationChecked.removeAll()
                    for i in 0..<data.count{
                        self.location.append((data[i] as AnyObject).value(forKey: "name") as! String)
                        self.locationIDArray.append("\((data[i] as AnyObject).value(forKey: "id") as! NSNumber)")
                        self.locationChecked.append((data[i] as AnyObject).value(forKey: "status") as! String)
                    }
                    self.locationView.updateList(self.location)
                }
            }
            //            else{
            //                self.ShowAlertView(title: "Conselho Fashion", message: dic.value(forKey: "message") as! String, viewController: self)
            //            }
        }
    }
}

//MARK:-
@available(iOS 11.0, *)
extension ProfileController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.LocationArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = locationCollectionView.dequeueReusableCell(withReuseIdentifier: "ProfilelocationCell", for: indexPath) as! ProfilelocationCell
        let locationDict = self.LocationArray[indexPath.row]
        cell.locationText.text = (locationDict["Location"] as! String)
        cell.crossBtn.tag = indexPath.row
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cell = locationCollectionView.dequeueReusableCell(withReuseIdentifier: "ProfilelocationCell", for: indexPath) as! ProfilelocationCell
        let locationDict = self.LocationArray[indexPath.row]
        cell.locationText.text = (locationDict["Location"] as! String)
        cell.locationText.sizeToFit()
        return CGSize(width: cell.locationText.frame.width + 25, height: 40)
    }
    
}

class ProfilelocationCell : UICollectionViewCell{
    @IBOutlet weak var locationText: UILabel!
    @IBOutlet weak var crossBtn: UIButton!
}

//struct Test :  Codable {
//    var username: String?
//    var phone_number: String?
//    var user_Location: [[String:Any]] = []
//    var cpf_number: String?
//}
