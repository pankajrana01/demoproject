//
//  SettingsController.swift
//  Conselho Fashion
//
//  Created by apple on 06/05/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

@available(iOS 11.0, *)
class SettingsController: UIViewController {
    
    var imgArr = [UIImage]()
    var lblname = [String]()
    var socialArray = [String]()
    var socialImageArray = [UIImage]()
    var mylanguageCode = "pt-BR"
    override func viewDidLoad() {
        super.viewDidLoad()
        addNavigationBarImage()
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.title = "Settings".localized(loc: mylanguageCode)
        imgArr = [UIImage(named: "Asset 7"),UIImage(named: "Asset 8"),UIImage(named: "Asset 9"),UIImage(named: "Asset 30"),#imageLiteral(resourceName: "garbage")] as! [UIImage]
        lblname = ["ChangePassword".localized(loc: mylanguageCode),"Terms".localized(loc: mylanguageCode),"Privacy".localized(loc: mylanguageCode),"Suporte","TerminateAccount".localized(loc: mylanguageCode)]
        socialArray  = ["Terms".localized(loc: mylanguageCode),"Privacy".localized(loc: mylanguageCode),"TerminateAccount".localized(loc: mylanguageCode)]
        socialImageArray = [UIImage(named: "Asset 8"),UIImage(named: "Asset 9"),#imageLiteral(resourceName: "garbage")] as! [UIImage]
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
       addNavigationBarImage()
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.title = "Settings".localized(loc: mylanguageCode)
    }
    
    @IBAction func logout(_ sender: UIBarButtonItem) {
        // Show alert View
        let alertController = UIAlertController(title: "Conselho Fashion", message: "LogoutMessage".localized(loc: mylanguageCode), preferredStyle: .alert)
        
        
        let declineAction = UIAlertAction(title: "Decline".localized(loc: mylanguageCode), style: .cancel, handler: nil)
        alertController.addAction(declineAction)
        
        let acceptAction = UIAlertAction(title: "Accept".localized(loc: mylanguageCode), style: .default) { (_) -> Void in
          
            self.logout()  // logout function call
            
        }
        alertController.addAction(acceptAction)
        
        present(alertController, animated: true, completion: nil)

        }
    func logout(){
        self.showProgress()
        networkServices.shared.getDataWithoutParameter(methodname: methodsName.userCase.logout.caseValue) { (response) in
            print (response)
            let dic = response as! NSDictionary
            print(dic)
            if (dic ).value(forKey: "success") as! Bool == true
            {
                    UserDefaults.standard.removeObject(forKey: "userid")
                    UserDefaults.standard.removeObject(forKey: "usertoken")
                    UserDefaults.standard.removeObject(forKey: "socialValue")
                    self.performPushSeguefromController(identifier: "SignInController")
                    self.hideProgress()
                }
            else{
                self.hideProgress()
                self.ShowAlertView(title: "Conselho Fashion", message: (dic).value(forKey: "message") as! String, viewController: self)
            }
            
            
        }
       }
    //MARK:- Terminate Account API Call
    func TerminateAccount(){
        networkServices.shared.getDataWithoutParameter(methodname: methodsName.userCase.TerminateAccount.caseValue) { (response) in
            print(response)
            let dic = response as! NSDictionary
            if dic.value(forKey: "success") as! Bool == true{
                // Show alert View
                let alertController = UIAlertController(title: "Conselho Fashion", message: "AccountDeleted".localized(loc: self.mylanguageCode), preferredStyle: .alert)
                  let acceptAction = UIAlertAction(title: "OK", style: .default) { (_) -> Void in
                    UserDefaults.standard.removeObject(forKey: "userid")
                    UserDefaults.standard.removeObject(forKey: "usertoken")
                   // UserDefaults.standard.removeObject(forKey: "socialValue")
                    let Home = self.storyboard?.instantiateViewController(withIdentifier: "SignInController") as! SignInController
                    self.present(Home, animated:true, completion:nil)
                }
                alertController.addAction(acceptAction)
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    
    }
@available(iOS 11.0, *)
extension SettingsController:UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if UserDefaults.standard.value(forKey: "socialValue") as? String == "5"{
            return socialArray.count
        }
        else{
        return lblname.count
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingCell") as! SettingCell
        if UserDefaults.standard.value(forKey: "socialValue") as? String == "5" {
            cell.lbl.text = socialArray[indexPath.row]
            cell.img.image = socialImageArray[indexPath.row]
            if indexPath.row == 3{
              cell.lineView.isHidden = true
            }
        }
        else{
        cell.lbl.text = lblname[indexPath.row]
        cell.img.image = imgArr[indexPath.row]
        if indexPath.row == 4{
            cell.lineView.isHidden = true
            }}
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if UserDefaults.standard.value(forKey: "socialValue") as? String == "5"{
            if indexPath.row == 0{
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "Terms_ConditionController") as! Terms_ConditionController
                vc.titleName = "Terms".localized(loc: mylanguageCode)
                self.navigationController?.pushViewController(vc, animated: true)
            }else if indexPath.row == 1{
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "Terms_ConditionController") as! Terms_ConditionController
                vc.titleName = "Privacy".localized(loc: mylanguageCode)
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        }
        else{
        if indexPath.row == 0{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChangePasswordController") as! ChangePasswordController
            self.navigationController?.pushViewController(vc, animated: true)
            }else if indexPath.row == 1{
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "Terms_ConditionController") as! Terms_ConditionController
            vc.titleName = "Terms".localized(loc: mylanguageCode)
            self.navigationController?.pushViewController(vc, animated: true)
        }else if indexPath.row == 2{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "Terms_ConditionController") as! Terms_ConditionController
            vc.titleName = "Privacy".localized(loc: mylanguageCode)
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if indexPath.row == 3{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SupportsController") as! SupportsController
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else {
            // Show alert View
            let alertController = UIAlertController(title: "Conselho Fashion", message: "TerminateMessage".localized(loc: mylanguageCode), preferredStyle: .alert)
            
            let acceptAction = UIAlertAction(title: "yes".localized(loc: mylanguageCode), style: .default) { (_) -> Void in
                self.TerminateAccount() // Terminate Account API Call
            }
            alertController.addAction(acceptAction)
            
            let Decline = UIAlertAction(title: "No".localized(loc: mylanguageCode), style: .default) { (_) -> Void in
                alertController.dismiss(animated: true, completion: nil)
              }
            alertController.addAction(Decline)
            present(alertController, animated: true, completion: nil)
            }
        }
    }
    
}
class SettingCell:UITableViewCell{
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var lbl: UILabel!
}
