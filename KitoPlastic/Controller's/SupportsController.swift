//
//  SupportsController.swift
//  Conselho Fashion
//
//  Created by apple on 06/05/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

@available(iOS 11.0, *)
class SupportsController: UIViewController,LBZSpinnerDelegate {
    @IBOutlet weak var spinnerView: LBZSpinner!
    @IBOutlet weak var NavigationViewHeight: NSLayoutConstraint!
    @IBOutlet weak var Commentview: DesignableTextView!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var SubmitBtn: UIButton!
    @IBOutlet weak var supportText: UILabel!
    
    var TittleArray : [String] = []
    var TittleIDArray : [String] = []
    var tittleid : String = ""
    var SubmitText : String = ""
    var languagecode = "pt-BR"
    func spinnerChoose(_ spinner: LBZSpinner, index: Int, value: String) {
        var spinnerName = ""
        if spinner == spinnerView { spinnerName = "ChooseQuestion" .localized(loc: languagecode)}
        if spinner == spinnerView {
            print("Spinner : \(spinnerName) : { Index : \(index) - \(value) }")
            //self.selectedState_id = "\(self.state_idArry[index])"
            tittleid = TittleIDArray[index]
            spinnerView.text = value
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // addNavigationBarImage()
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.title = "Suporte"
        let screenSize = UIScreen.main.bounds
        if screenSize.height <= 736{
            NavigationViewHeight.constant = 64
        }else{
            NavigationViewHeight.constant = 88
        }
        // cancelBtn.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        languageChange()
         getSupportTittle() //Api Call
        Commentview.text = "CommentWrite".localized(loc: languagecode)
        Commentview.textColor = UIColor.lightGray
        spinnerView.delegate = self
        if spinnerView.selectedIndex == LBZSpinner.INDEX_NOTHING {
            print("NOTHING VALUE")
            spinnerView.text = "ChooseQuestion".localized(loc: languagecode)
         }
    }
    //MARk:- languageChange Function
    func languageChange(){
        supportText.text = "Support".localized(loc: languagecode)
        SubmitBtn.setTitle("Submit".localized(loc: languagecode), for: .normal)
       // cancelBtn.setTitle("Cancel".localized(loc: languagecode), for: .normal)
    }
    
    @IBAction func cancelBtnTap(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func BackBtnTap(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func submitTap(_ sender: UIButton) {
        if spinnerView.text == "ChooseQuestion".localized(loc: languagecode){
            ShowAlertView(title: "Conselho Fashion", message: "QuestionAlert".localized(loc: languagecode), viewController: self)
        }
       else if Commentview.text == "CommentWrite".localized(loc: languagecode){
            ShowAlertView(title: "Conselho Fashion", message: "RemarksAlert".localized(loc: languagecode), viewController: self)
        }
          else {
            SubmitComment() //API Call
        }
        
    }
    
    //MARK:- Get Support Tittle
    func getSupportTittle(){
        
        networkServices.shared.getDataWithoutParameter(methodname: methodsName.userCase.GetSupporttittle.caseValue) { (response) in
            print(response)
            let dic = response as! NSDictionary
            if dic.value(forKey: "success") as! Bool == true{
                if let data = dic.value(forKey: "data") as? NSArray{
                    print(data)
                    for i in 0..<data.count{
                        self.TittleArray.append((data[i] as AnyObject).value(forKey: "title") as! String)
                        self.TittleIDArray.append("\((data[i]as AnyObject).value(forKey: "id") as! NSNumber)")
                    }
                    self.spinnerView.updateList(self.TittleArray)
                }
            }
            else{
                self.ShowAlertView(title: "Conselho Fashion", message: dic.value(forKey: "message") as! String, viewController: self)
            }
        }
    }
    //MARK:- Submit Support Comment API
    func SubmitComment(){
        self.showProgress()
        let parameter = [
            "support_id" : tittleid,
            "remarks" : Commentview.text!
        ]
        
        networkServices.shared.postDatawithHeader(methodName: methodsName.userCase.submitSupport.caseValue, parameter: parameter) { (response) in
            print(response)
            self.hideProgress()
            let dic  = response as! NSDictionary
            if  dic.value(forKey: "success") as! Bool == true {

                // Show alert View
                let alertController = UIAlertController(title: "Conselho Fashion", message: dic.value(forKey: "message") as! String, preferredStyle: .alert)
                let acceptAction = UIAlertAction(title: "OK", style: .default) { (_) -> Void in
                    self.Commentview.text = "CommentWrite".localized(loc: self.languagecode)
                    self.spinnerView.text = "ChooseQuestion".localized(loc: self.languagecode)
                    self.Commentview.textColor = UIColor.lightGray
                    self.navigationController?.popViewController(animated: true)
                    
                }
                alertController.addAction(acceptAction)
                self.present(alertController, animated: true, completion: nil)
            }
            else{
               self.ShowAlertView(title: "Conselho Fashion", message: dic.value(forKey: "message") as! String, viewController: self)
            }
        }
    }
    
    
}

@available(iOS 11.0, *)
extension SupportsController : UITextViewDelegate{
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if Commentview.textColor == UIColor.lightGray {
            Commentview.text = nil
            Commentview.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if Commentview.text.isEmpty {
            Commentview.text = "CommentWrite".localized(loc: languagecode)
            Commentview.textColor = UIColor.lightGray
        }
    }
}
