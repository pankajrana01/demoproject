//
//  TabBarController.swift
//  KitoPlastic
//
//  Created by apple on 06/05/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

@available(iOS 11.0, *)
class TabBarController: UIViewController,profileViewDelegate{
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? MySpaceController
        {
            destination.callDelegate = self
        }
    }
    func addTabBarView(tap: Int) {
        selectedIndex = tap
        buttons[selectedIndex].isSelected = true
        didTapOnBar(buttons[selectedIndex])
    }
    
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet var buttons: [UIButton]!
    @IBOutlet weak var mySpaceBtn: UIButton!
    @IBOutlet weak var mySpacelbl: UILabel!
    @IBOutlet weak var compaignBtn: UIButton!
    @IBOutlet weak var compaignlbl: UILabel!
    @IBOutlet weak var giftBtn: UIButton!
    @IBOutlet weak var giftlbl: UILabel!
    @IBOutlet weak var NewsletterBtn: UIButton!
    @IBOutlet weak var newsLetterlbl: UILabel!
    @IBOutlet weak var plusBtn: UIButton!
    
    var MySpaceController1 = UIViewController()
    var CompaginController = UIViewController()
    var RedeemPricesController = UIViewController()
    var newsController = UIViewController()
    var GiftCardDetailController = UIViewController()
    var ProfileController = UIViewController()
    var viewControllers = [UIViewController]()
    var selectedIndex: Int = 0
    var mylanguageCode = "pt-BR"
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getProfile()
           self.navigationController?.isNavigationBarHidden = false
        
        let obj = MySpaceController()
        obj.callDelegate = self
        
        mySpaceBtn.setImage(UIImage(named: "mySpaceyellow"), for: .normal)
        mySpacelbl.textColor = YELLOW_COLOR
        
        compaignBtn.setImage(UIImage(named: "campaginWhite"), for: .normal)
        compaignlbl.textColor = UIColor.white
        
        giftBtn.setImage(UIImage(named: "redeemWhite"), for: .normal)
        giftlbl.textColor = UIColor.white
        
        NewsletterBtn.setImage(UIImage(named: "Asset 19"), for: .normal)
        newsLetterlbl.textColor = UIColor.white
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        MySpaceController1 = storyboard.instantiateViewController(withIdentifier: "MySpaceController")
        CompaginController = storyboard.instantiateViewController(withIdentifier: "CompaginController")
        RedeemPricesController = storyboard.instantiateViewController(withIdentifier: "RedeemPricesController")
        newsController = storyboard.instantiateViewController(withIdentifier: "NewsLetterViewController")
        GiftCardDetailController = storyboard.instantiateViewController(withIdentifier: "GiftCardDetailController")
        ProfileController = storyboard.instantiateViewController(withIdentifier: "ProfileController")
        
        viewControllers = [MySpaceController1,CompaginController,RedeemPricesController,newsController,GiftCardDetailController,ProfileController]
        
        
        //Set the Initial Tab when the App Starts.
        buttons[selectedIndex].isSelected = true
        didTapOnBar(buttons[selectedIndex])
    }
    override func viewWillAppear(_ animated: Bool) {
        changeStoryboardWithLanguage()
    }
    
    //MARK:- language translate
    func changeStoryboardWithLanguage(){
        mySpacelbl.text = "MySpace".localized(loc: mylanguageCode)
        compaignlbl.text = "Campaigns".localized(loc: mylanguageCode)
        giftlbl.text = "Redeem".localized(loc: mylanguageCode)
        newsLetterlbl.text = "novidades"
    }
    
    //MARK:- --------------------- Get profileData API----------------------------------------
    func getProfile(){
        networkServices.shared.postDataWithoutParameter(methodname: methodsName.userCase.getProfile.caseValue) { (response) in
            print(response)
            let dic = response as! NSDictionary
            print (dic)
               if dic.value(forKey: "success") as! Bool == true {
                    if let profileData = dic.value(forKey: "data") as? NSDictionary{
                       
                          let userType = (profileData.value(forKey: "user_type") as! String)
                         if userType == "1"{
                            self.plusBtn.isHidden = false
                         }
                             else{
                                self.plusBtn.isHidden = true
                            }
                            
                        }
                 }
                else{
                    self.hideProgress()
                
                }
            
        }
    }
    
    
    @IBAction func didTapOnBar(_ sender: UIButton) {
        //Get Access to the Previous and Current Tab Button.
        if sender.tag == 0{
            mySpaceBtn.setImage(UIImage(named: "mySpaceyellow"), for: .normal)
            mySpacelbl.textColor = YELLOW_COLOR
            
            compaignBtn.setImage(UIImage(named: "campaginWhite"), for: .normal)
            compaignlbl.textColor = UIColor.white
            
            giftBtn.setImage(UIImage(named: "redeemWhite"), for: .normal)
            giftlbl.textColor = UIColor.white
            
            NewsletterBtn.setImage(UIImage(named: "Asset 19"), for: .normal)
            newsLetterlbl.textColor = UIColor.white
            
        } else if sender.tag == 1{
            
            mySpaceBtn.setImage(UIImage(named: "mySpaceWhite"), for: .normal)
            mySpacelbl.textColor = UIColor.white
            
            compaignBtn.setImage(UIImage(named: "campaginYellow"), for: .normal)
            compaignlbl.textColor = YELLOW_COLOR
            
            giftBtn.setImage(UIImage(named: "redeemWhite"), for: .normal)
            giftlbl.textColor = UIColor.white
            
            NewsletterBtn.setImage(UIImage(named: "Asset 19"), for: .normal)
            newsLetterlbl.textColor = UIColor.white
            
        } else if sender.tag == 2{
            mySpaceBtn.setImage(UIImage(named: "mySpaceWhite"), for: .normal)
            mySpacelbl.textColor = UIColor.white
            
            compaignBtn.setImage(UIImage(named: "campaginWhite"), for: .normal)
            compaignlbl.textColor = UIColor.white
            
            giftBtn.setImage(UIImage(named: "redeemYellow"), for: .normal)
            giftlbl.textColor = YELLOW_COLOR
            
            NewsletterBtn.setImage(UIImage(named: "Asset 19"), for: .normal)
            newsLetterlbl.textColor = UIColor.white
        } else if sender.tag == 3{
            mySpaceBtn.setImage(UIImage(named: "mySpaceWhite"), for: .normal)
            mySpacelbl.textColor = UIColor.white
            
            compaignBtn.setImage(UIImage(named: "campaginWhite"), for: .normal)
            compaignlbl.textColor = UIColor.white
            
            giftBtn.setImage(UIImage(named: "redeemWhite"), for: .normal)
            giftlbl.textColor = UIColor.white
            
            NewsletterBtn.setImage(UIImage(named: "Asset 18"), for: .normal)
            newsLetterlbl.textColor = YELLOW_COLOR
        }
        
        
        
        selectedIndex = sender.tag
        let previousIndex = selectedIndex
        
        //Remove the Previous ViewController and Set Button State.
        buttons[previousIndex].isSelected = false
        
        let previousVC = viewControllers[previousIndex]
        previousVC.willMove(toParent: nil)
        previousVC.view.removeFromSuperview()
        previousVC.removeFromParent()
        
        //Add the New ViewController and Set Button State.
        sender.isSelected = true
        let vc = viewControllers[selectedIndex]
        addChild(vc)
        vc.view.frame = contentView.bounds
        contentView.addSubview(vc.view)
        vc.didMove(toParent: self)
    }
    @IBAction func plusBtn(_ sender: UIButton) {
        performPushSeguefromController(identifier: "AllCompaignViewController")
    }
    
    
}

