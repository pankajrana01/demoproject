//
//  Terms&ConditionController.swift
//  fashLOCO
//
//  Created by apple on 30/04/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import WebKit


@available(iOS 11.0, *)
class Terms_ConditionController: UIViewController,WKNavigationDelegate {
    
   
    
    @IBOutlet weak var textData: UITextView!
    var policyData = ""
    var titleName = ""
    var mylanguageCode = "pt-BR"
    var webView: WKWebView!
    
    override func loadView() {
        webView = WKWebView()
        webView.navigationDelegate = self
        view = webView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = titleName
        addNavigationBarImage()
        self.navigationController?.isNavigationBarHidden = false
        // Do any additional setup after loading the view.
        updateData() // function call
    }
    
    func updateData(){
        if titleName == "Terms".localized(loc: mylanguageCode){
            // MARK:- for testing server
           // let url = URL(string: "https://clientstagingdev.com/kinoplastic_server/api/terms_and_conditions")
            // MARK:-for live server
            let url = URL(string: "https://diggy.tech/api/terms_and_conditions")
            webView.load(URLRequest(url: url!))
            webView.allowsBackForwardNavigationGestures = true
//            UIWebView.loadRequest(werbView)(NSURLRequest(url: NSURL(string: "http://68.183.74.38:8008/joao/api/terms_and_conditions")! as URL) as URLRequest)
        }
        else{
           // let url = URL(string: "https://clientstagingdev.com/kinoplastic_server/api/privacy_policy")
            let url = URL(string: "https://diggy.tech/api/privacy_policy")
            webView.load(URLRequest(url: url!))
            webView.allowsBackForwardNavigationGestures = true
//            UIWebView.loadRequest(werbView)(NSURLRequest(url: NSURL(string: "http://68.183.74.38:8008/joao/api/privacy_policy")! as URL) as URLRequest)
        }
    }
}


